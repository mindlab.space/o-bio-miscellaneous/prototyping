# o-bio prototyping

Prototyping of the O-Bio platform.  

O-Bio is an opensource platform for biosignals and IoT applications.

[The source code repository](https://gitlab.com/mindlab.space/o-bio-miscellaneous/prototyping)

**prerequisite:**  
- ubuntu    20.04
- python    3.8.5
- docker    19.03
- microk8s  1.20
- microk8s.helm3


## docker images

- [timeflux](./timeflux/docker/README.md) 
- [ohmycapitain](./ohmycapitain/docker/README.md)


## k8s

- See k8-helm/obio chart


## license

[mit license](./LICENSE)