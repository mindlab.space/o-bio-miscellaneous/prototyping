# docker image for ohmycapitain

a docker image that contains *ohmycapitain*.

## download the ready-made image

[The image on Docker Hub](https://hub.docker.com/r/quantumthinkers/ohmycapitain)  

download it locally:
```
$ docker pull quantumthinkers/obio-ohmycapitain
```
**Note**  
it won't work on local microk8s cluster unless the local cluster IP
is set in the file ```/root/envs/microk8s/client.config```   
it is recomended a local build, that will do this step automatically.  

## build the image yourself
Run ```make``` command for ```docker/ohmycapitain/Makefile```.
```
$ make build -f docker/ohmycapitain-server/Makefile
```
It will build the image: ```quantumthinkers/ohmycapitain```.   


## run it 
The followign command show how to run a container from 
the ```quantumthinkers/obio-ohmycapitain``` docker image. 

```
$ docker run -it --name obio-ohmycapitain \
    quantumthinkers/obio-timeflux bash
```

## about ohmycapitain
ohmycapitain is a service (with API) that manages a kubernetes cluster from the 
inside, by sending commands to the hosting cluster's API.  
It is meant to be running in a pod, inside the cluster to be managed.  
It uses kubernetes's [python client](https://github.com/kubernetes-client/python).   
More information at: https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/  

O-Bio is an opensource platform for biosignals and IoT applications.  
Code repository: https://gitlab.com/mindlab.space/o-bio-miscellaneous/prototyping  