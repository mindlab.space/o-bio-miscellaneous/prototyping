"""
Sobstitute the cluster IP in the client.config file, using the cluster's
node internal IP from the cluster-info dump.
"""

import sys, os, json, yaml

# absolute path of the script
pathname = os.path.dirname(sys.argv[0])  

# read the internal IP address of the k8s node
configfile = os.path.abspath(pathname + '/envs/microk8s/cluster-info/nodes.json')
with open(configfile) as f:
  data = json.load(f)
internal_address = data["items"][0]["status"]["addresses"][0]["address"]

# sobstitute the value into the client.config file
configfile = os.path.abspath(pathname + '/envs/microk8s/client.config')
with open(configfile) as f:
  yaml_data = yaml.safe_load(f)
server_address = yaml_data["clusters"][0]["cluster"]["server"]
server_address = server_address.split(':')
yaml_data["clusters"][0]["cluster"]["server"] = (server_address[0] + "://" 
    + internal_address + ":" + server_address[2] )
with open(pathname + '/envs/microk8s/client.config', 'w') as file:
  documents = yaml.dump(yaml_data, file)