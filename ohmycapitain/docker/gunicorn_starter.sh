#!/bin/env sh
gunicorn \
  --chdir ./api/ \
  --bind 0.0.0.0:80 \
  --workers=2 \
  --threads=4 \
  --timeout 300 \
  --worker-tmp-dir /dev/shm \
  --error-logfile=/var/log/gunicorn/gunicorn-error.log \
  --access-logfile /var/log/gunicorn/gunicorn-access.log \
  --log-level info \
  --capture-output \
  --enable-stdio-inheritance \
  --reload \
  wsgi:app   