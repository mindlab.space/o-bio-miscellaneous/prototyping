import sys, os
from kubernetes import client, config

pathname = os.path.dirname(sys.argv[0])  
configfile = os.path.abspath(pathname + '../../../client.config')
print(configfile)
config.load_kube_config(configfile)

v1=client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_pod_for_all_namespaces(watch=False)
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
