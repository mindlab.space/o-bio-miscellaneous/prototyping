# development 

A readme file about O-Bio prototyping and development


## ohmycapitain-server

Setup the python virtual environment:
```
$ apt-get install -y python3-venv 
$ python3 -m venv obio-prototyping
$ source obio-prototyping/bin/activate
```

Deactivate the venv:
```
$ deactivate
```
