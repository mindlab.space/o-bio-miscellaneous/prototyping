# TODOs

- Docker image Timeflux
    - Make the docker image lighter
        - Form Alpine
    - Use another user other than root


- Docker image ohmycapitain-server
    - Make the docker image lighter
        - Form Alpine
    - Use another user other than root
    - change systemd
        - to not use root user
        - to use a python virtual env (https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04)
        