# docker image for timeflux

A docker image with Timeflux that can use the host's USB bluetooth 
dongle device.

## download the ready-made image

[The image on Docker Hub](https://hub.docker.com/r/quantumthinkers/obio-timeflux)  

Download it locally:
```
$ docker pull quantumthinkers/obio-timeflux
```

## build the image yourself
Run ```make``` command for ```docker/timeflux/Makefile```.
```
$ make build -f docker/timeflux/Makefile
```
It will build the image: ```quantumthinkers/obio-timeflux```.   

The timeflux graph files (yaml files) in the folder ```graphs``` will
be available into the timeflux docker image.

## run it 
The followign command show how to run a container from 
the ```quantumthinkers/obio-timeflux``` docker image, 
use the host's bluetooth devices (OpenBCI USB bluetooth dongle), and
print data on screen.
The USB bluetooth dongle must be plugged in before running the command.

```
$ docker run -i --device=/dev/ttyACM0 -a stdout -a stderr \ 
    quantumthinkers/obio-timeflux timeflux -d graphs/my-ganglion.yml
```

## about timeflux
Timeflux is a free and open-source framework for the acquisition and real-time processing of biosignals.  
Docs: https://doc.timeflux.io/  
Source code: https://github.com/timeflux/