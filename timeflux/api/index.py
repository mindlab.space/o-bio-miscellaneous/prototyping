from flask import (Flask, render_template)
import os, subprocess

app = Flask(__name__)

script_dir = os.path.dirname( os.path.abspath(__file__) )
graph_dir = os.path.abspath( script_dir + '/../graphs')

@app.route("/")
def root():
  return "timeflux API works!"

@app.route("/timeflux-start")
def timeflux_start():
  subprocess.Popen([ script_dir + '/timeflux_start.sh', graph_dir + '/synthetic.yaml'])
  return "timeflux started" 

@app.route("/timeflux-kill")
def timeflux_kill():
  subprocess.Popen([ script_dir + '/timeflux_kill.sh'])
  return "timeflux killed" 

if __name__ == "__main__":
  app.run(host='0.0.0.0')